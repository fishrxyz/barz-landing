# Creating a pipeline user for barz.lol

## Step 1: Create a group

```bash
aws iam create-group --group-name my-group
```

## Step 2: Create a pipeline user

```bash
aws iam create-user --user-name pipeline-user --path "/barz/"

```

## Step 3: Add user to group

```bash
aws iam add-user-to-group --user-name pipeline-user --group-name my-group
aws iam get-group --group-name my-group # to verify that the user is part of the group
```

## Step 4: Create the user's access keys

```bash
aws iam create-access-key --user-name pipeline-user
```

## Step 6: Create a policy and attach it to the user

```bash
# create is using the aws poligy generator
aws iam create-policy --policy-name my-policy --policy-document file://path-to-policy.json
```

## Step 7: Attach newly created policy to user

```bash
aws iam list-policies --query 'Policies[?PolicyName=="my-policy-name"].Arn' --output text # get arn of newly created policy
aws iam attach-user-policy --user-name pipeline-user --policy-arn my-policy-arn
```
